//
//  SecondViewController.swift
//  treadMil
//
//  Created by Marc Michel on 10/14/18.
//  Copyright © 2018 Marc Michel. All rights reserved.
//

import UIKit
import RealmSwift

class LogVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
  
    
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Run.fetchData()?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "LogCell") as? LogCell else {return UITableViewCell() }
        guard let run = Run.fetchData()?[indexPath.row] else {return LogCell()}
        cell.configurecell(run: run)
        
        return cell
    }


}

