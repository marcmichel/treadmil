
//  treadMil
//
//  Created by Marc Michel on 10/14/18.
//  Copyright © 2018 Marc Michel. All rights reserved.
//

import UIKit
import MapKit
import RealmSwift

class RunVC: LocationVC {

    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var avgPace: UILabel!
    @IBOutlet weak var distance: UILabel!
    @IBOutlet weak var duration: UILabel!
    @IBOutlet weak var closeBtn: UIButton!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var viewAlpha: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        checkLocationAuthStatus()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        manager?.delegate = self
        mapView.delegate = self
        manager?.startUpdatingLocation()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        setupMapView()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        manager?.stopUpdatingLocation()
    }
    
    func setupMapView() {
        if let overlay = addLastRuntoMap() {
            if mapView.overlays.count > 0 {
                mapView.removeOverlays(mapView.overlays)
            }
            mapView.addOverlay(overlay)
            viewAlpha.isHidden = false
            closeBtn.isHidden = false
            stackView.isHidden = false
        } else {
            viewAlpha.isHidden = true
            closeBtn.isHidden = true
            stackView.isHidden = true
            centerMaponUserLocation()
        }
    }
    
    func addLastRuntoMap() -> MKPolyline? {
        guard let lastRun = Run.fetchData()?.last else { return nil }
        avgPace.text = lastRun.pace.format()
        duration.text = lastRun.duration.format()
        distance.text = "\(lastRun.distance.metersToMiles(decimalPlace: 2)) mi"
        var coordinates = [CLLocationCoordinate2D]()
        for location in lastRun.locations {
            coordinates.append(CLLocationCoordinate2D(latitude: location.latitude, longitude: location.longitude))
        }
        
        mapView.userTrackingMode = .none
        mapView.setRegion(centerMapOnPreviousRoute(locations: lastRun.locations), animated: true)
        
        return MKPolyline(coordinates: coordinates, count: lastRun.locations.count)
    }
    
    
    @IBAction func startRunningBtn(_ sender: Any) {
        guard let timeVC = storyboard?.instantiateViewController(withIdentifier: "TimeVC") as? TimeVC else {return}
        present(timeVC, animated: true, completion: nil)
    }
    
    func centerMaponUserLocation() {
        mapView.userTrackingMode = .follow
        let coordinateRegion = MKCoordinateRegion(center: mapView.userLocation.coordinate, latitudinalMeters: 1000, longitudinalMeters: 1000)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    func centerMapOnPreviousRoute(locations: List<Locations>) -> MKCoordinateRegion {
        guard let initialLoc =  locations.first else { return MKCoordinateRegion()}
        var minLongitude = initialLoc.longitude
        var minLatitude =  initialLoc.latitude
        var maxLon = minLongitude
        var maxLat = minLatitude
        
        for location in locations {
            minLatitude = min(minLatitude, location.latitude)
            minLongitude = min(minLongitude, location.latitude)
            maxLat = max(maxLat, location.latitude)
            maxLon = max(maxLon, location.longitude)

        }
        return MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: (minLatitude + maxLat) / 2, longitude: (minLongitude + maxLon) / 2), span: MKCoordinateSpan(latitudeDelta: (maxLat - minLatitude) * 1.4, longitudeDelta: (maxLon - minLongitude) * 1.4))
    }
    
    @IBAction func locationButton(_ sender: Any) {
        centerMaponUserLocation()
    }
    
    @IBAction func closeBtn(_ sender: Any) {
        self.viewAlpha.isHidden = true
        self.closeBtn.isHidden = true
        self.stackView.isHidden = true
        centerMaponUserLocation()
    }
}

extension RunVC: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            checkLocationAuthStatus()
            mapView.showsUserLocation = true
        }
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let polyline = overlay as! MKPolyline
        let renderer = MKPolylineRenderer(polyline: polyline)
        renderer.strokeColor = #colorLiteral(red: 0.5568627715, green: 0.3529411852, blue: 0.9686274529, alpha: 1)
        renderer.lineWidth = 4
        return renderer
    }
}

