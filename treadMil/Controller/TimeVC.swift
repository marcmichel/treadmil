//
//  TimeVC.swift
//  treadMil
//
//  Created by Marc Michel on 10/18/18.
//  Copyright © 2018 Marc Michel. All rights reserved.
//

import UIKit
import MapKit
import RealmSwift

class TimeVC: LocationVC {

    @IBOutlet weak var pauseBtnAction: UIButton!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var paceLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var backgroundSwipe: UIImageView!
    @IBOutlet weak var endRunLabel: UILabel!
    @IBOutlet weak var constraintToAnimate: NSLayoutConstraint!
    @IBOutlet weak var pauseBtn: UIImageView!
    
    var startLocation: CLLocation!
    var lastLocation: CLLocation!
    var timer = Timer()
    var coordinateLocations = List<Locations>()
    var runDistance = 0.0
    var pace = 0
    var counter = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let swipeGesture = UIPanGestureRecognizer(target: self, action: #selector(endRunSwipe(sender:)))
        pauseBtn.addGestureRecognizer(swipeGesture)
        pauseBtn.isUserInteractionEnabled = true
        swipeGesture.delegate = self as? UIGestureRecognizerDelegate
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        manager?.delegate = self
        manager?.distanceFilter = 10
        startRun()
    }
    
    func startRun() {
        manager?.startUpdatingLocation()
        startTimer()
        pauseBtnAction.setImage(#imageLiteral(resourceName: "pauseButton"), for: .normal)
    }
    
    func pauseRun() {
        startLocation = nil
        lastLocation = nil
        timer.invalidate()
        manager?.stopUpdatingLocation()
        pauseBtnAction.setImage(#imageLiteral(resourceName: "resumeButton"), for: .normal)
    }
    
    func endRun() {
        manager?.stopUpdatingLocation()
        Run.saveData(pace: pace, distance: runDistance, duration: counter, location: coordinateLocations)
    }
    
    func startTimer() {
        durationLabel.text = counter.format()
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateCounter), userInfo: nil, repeats: true)
    }
    
    @objc func updateCounter() {
        counter += 1
        durationLabel.text = counter.format()
    }
    
    func calculatePace(time second: Int, miles: Double) -> String {
        pace = Int(Double(second) / miles)
        return pace.format()
    }
    
    @IBAction func pauseBtnPressed(_ sender: Any) {
        if timer.isValid {
            pauseRun()
        } else {
            startRun()
        }
    }
    
    
    @objc func endRunSwipe(sender: UIPanGestureRecognizer) {
        let minAdjust: CGFloat = 60
        let maxAdjust: CGFloat = 116.5
        
        if let sliderView = sender.view {
            if sender.state == UIGestureRecognizer.State.began || sender.state == UIPanGestureRecognizer.State.changed {
                let translation = sender.translation(in: self.view)
                if sliderView.center.x >= (backgroundSwipe.center.x - minAdjust) && sliderView.center.x <= (backgroundSwipe.center.x + maxAdjust) {
                    sliderView.center.x = sliderView.center.x + translation.x
                } else if sliderView.center.x >= (backgroundSwipe.center.x + maxAdjust) {
                    sliderView.center.x = backgroundSwipe.center.x + maxAdjust
                    endRun()
                    dismiss(animated: true, completion: nil)
                } else {
                    sliderView.center.x = backgroundSwipe.center.x - minAdjust
                }
                sender.setTranslation(CGPoint.zero, in: self.view)
            } else if sender.state == UIGestureRecognizer.State.ended {
                UIView.animate(withDuration: 0.1) {
                    sliderView.center.x = self.backgroundSwipe.center.x - minAdjust
                }
            }
        }
    }
}

extension TimeVC: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            checkLocationAuthStatus()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if startLocation == nil {
            startLocation = locations.first
        } else if let location = locations.last {
            runDistance += lastLocation.distance(from: location)
            let newLocation = Locations(longitude: Double(lastLocation.coordinate.longitude), latitude: Double(lastLocation.coordinate.latitude))
            coordinateLocations.insert(newLocation, at: 0)
            distanceLabel.text = "\(runDistance.metersToMiles(decimalPlace: 2))"
            if counter > 0 && runDistance > 0 {
                paceLabel.text = calculatePace(time: counter, miles: runDistance.metersToMiles(decimalPlace: 2))
            }
        }
        
        lastLocation = locations.last
    }
}
