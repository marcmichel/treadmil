//
//  Constants.swift
//  treadMil
//
//  Created by Marc Michel on 10/25/18.
//  Copyright © 2018 Marc Michel. All rights reserved.
//

import Foundation

let REALM_QUEUE = DispatchQueue(label: "realmQueue")

let REALM_CONFIG = "realmConfig"
