//
//  RealmConfig.swift
//  treadMil
//
//  Created by Marc Michel on 11/4/18.
//  Copyright © 2018 Marc Michel. All rights reserved.
//

import Foundation
import RealmSwift

class RealmConfig {
    
    static var runDataConfig: Realm.Configuration {
        let realmPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent(REALM_CONFIG)
        let config = Realm.Configuration(
            fileURL: realmPath,
            schemaVersion: 0,
            migrationBlock: { migration, oldSchemaVersion in
                if(oldSchemaVersion < 0 ) {
                    //nothing to do
                    //realm will auto update
            }
        })
        return config
    }
}
