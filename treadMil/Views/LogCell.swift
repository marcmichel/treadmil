//
//  LogCell.swift
//  treadMil
//
//  Created by Marc Michel on 10/25/18.
//  Copyright © 2018 Marc Michel. All rights reserved.
//

import UIKit

class LogCell: UITableViewCell {
    
    @IBOutlet weak var paceLabel: UILabel!
    @IBOutlet weak var milesRanLabel: UILabel!
    @IBOutlet weak var runDuration: UILabel!
    @IBOutlet weak var dateStampLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configurecell(run: Run) {
        runDuration.text = run.duration.format()
        milesRanLabel.text = "\(run.distance.metersToMiles(decimalPlace: 2))"
        dateStampLabel.text = run.date.getDateString()
        paceLabel.text = run.pace.format()
        
    }
    
}
