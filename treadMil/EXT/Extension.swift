//
//  TwoDecimalPlacesExtension.swift
//  treadMil
//
//  Created by Marc Michel on 10/25/18.
//  Copyright © 2018 Marc Michel. All rights reserved.
//

import Foundation

extension Double {
    func metersToMiles(decimalPlace places: Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return ((self / 1609.34) * divisor).rounded() / divisor
    }
}

extension Int {
    func format() -> String {
        let durationHours = self / 3600
        let durationMinutes = (self % 3600) / 60
        let durationSecond = (self % 3600) % 60
        
        if durationSecond < 0 {
            return "00:00:00"
        } else {
            if durationHours == 0 {
                return String(format: "%02d:%02d", durationMinutes, durationSecond)
            } else {
                return String(format: "%02d:%02d:%02d", durationHours, durationMinutes, durationSecond)
            }
        }
    }
}

extension NSDate {
    func getDateString() -> String {
        let calendar = Calendar.current
        let month = calendar.component(.month, from: self as Date)
        let day = calendar.component(.day, from: self as Date)
        let year = calendar.component(.year, from: self as Date)
        return "\(month)/\(day)/\(year)"
    }
}
