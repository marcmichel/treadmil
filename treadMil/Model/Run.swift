//
//  Run.swift
//  treadMil
//
//  Created by Marc Michel on 10/25/18.
//  Copyright © 2018 Marc Michel. All rights reserved.
//

import Foundation
import RealmSwift

class Run: Object {
    @objc dynamic public private(set) var id = ""
    @objc dynamic public private(set) var date = NSDate()
    @objc dynamic public private(set) var pace = 0
    @objc dynamic public private(set) var distance = 0.0
    @objc dynamic public private(set) var duration = 0
    public private(set) var locations = List<Locations>()
    
    override class func primaryKey() -> String {
        return "id"
    }
    
    override class func indexedProperties() -> [String] {
      return ["pace", "date", "duration"]
    }
    
    convenience init(pace: Int, distance: Double, duration: Int, location: List<Locations>) {
        self.init()
        self.id = UUID().uuidString.lowercased()
        self.date = NSDate()
        self.locations = location
        self.pace = pace
        self.distance = distance
        self.duration = duration
    }
    
    static func saveData(pace: Int, distance: Double, duration: Int, location: List<Locations>) {
        REALM_QUEUE.sync {
            let run = Run(pace: pace, distance: distance, duration: duration, location: location)
            do {
                let realm = try Realm(configuration: RealmConfig.runDataConfig)
                try realm.write {
                    realm.add(run)
                    try realm.commitWrite()
                }
            } catch {
                debugPrint("error \(error.localizedDescription)")
            }
        }
    }
    
    static func fetchData() -> Results<Run>? {
        do {
            let realm = try Realm(configuration: RealmConfig.runDataConfig)
            var runs = realm.objects(Run.self)
            runs = runs.sorted(byKeyPath: "date", ascending: true)
            return runs
        } catch {
            return nil
        }
    }
}
