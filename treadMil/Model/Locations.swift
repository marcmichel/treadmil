//
//  Locations.swift
//  treadMil
//
//  Created by Marc Michel on 11/4/18.
//  Copyright © 2018 Marc Michel. All rights reserved.
//

import Foundation
import RealmSwift

class Locations: Object {
    @objc dynamic public private(set) var longitude = 0.0
    @objc dynamic public private(set) var latitude = 0.0
    
    convenience init(longitude: Double, latitude: Double) {
        self.init()
        self.longitude = longitude
        self.latitude = latitude
    }
}
